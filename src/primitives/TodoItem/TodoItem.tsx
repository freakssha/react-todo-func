import React from 'react';
import './TodoItem.sass';
import {ITodo} from "../../TodoApp";


interface TodoListProps {
    todo: ITodo,
    selectTodo: (e: React.FormEvent<HTMLInputElement>) => void,
    editTodo: (e: React.FormEvent<HTMLInputElement>) => void,
}


function TodoItem({todo, selectTodo, editTodo}: TodoListProps) {
    return (
        <div className='todo'>
            <input type='checkbox'
                   id={todo.id}
                   checked={todo.isChecked}
                   onChange={selectTodo}
            />
            <input className='todo-content'
                   id={todo.id}
                   value={todo.name}
                   onChange={editTodo}
            />
        </div>
    )
}

export default React.memo(TodoItem);




