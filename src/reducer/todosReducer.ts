import {ITodo, todosFunctions} from "../TodoApp";

interface IAction {
    type: string | string[],
    payload: any
}

export default function todosReducer(state: ITodo[], action: IAction): ITodo[] {
    console.log(action.payload)
    switch (action.type) {
        case todosFunctions.ADD:
            return [{
                id: Date().valueOf() + Math.random(),
                name: action.payload.newTaskText,
                isChecked: false
            }, ...action.payload.todos]
        case todosFunctions.EDIT:
            return action.payload.todos.map((todo: ITodo) => (
                    todo.id === action.payload.id ? Object.assign({}, todo, {name: action.payload.value}) : todo
                )
            )
        case todosFunctions.SELECT:
            return action.payload.todos.map((todo: ITodo) => (
                    todo.id === action.payload.id ? Object.assign({}, todo, {isChecked: [!todo.isChecked]}) : todo
                )
            )
        case todosFunctions.SELECT_ALL:
            return action.payload.todos.map((todo: ITodo) => (
                    Object.assign({}, todo, {isChecked: true})
                )
            )
        case todosFunctions.DELETE:
            return action.payload.todos.filter((todo: ITodo) =>
                !todo.isChecked
            )
        default:
            return action.payload
    }
}