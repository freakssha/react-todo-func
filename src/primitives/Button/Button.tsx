import React, {MouseEventHandler} from "react";
import './Button.sass';


interface ButtonProps {
    onClick: MouseEventHandler<HTMLButtonElement>,
    content: string,
}

function Button({onClick, content}: ButtonProps) {
    return (
        <button onClick={onClick}>
            {content}
        </button>
    )
}


export default React.memo(Button);





