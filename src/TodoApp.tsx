import React, {useCallback, useReducer, useState} from 'react';
import './TodoApp.sass';
import {exampleTodos} from './data';
import TodoList from "./components/TodoList/TodoList";
import AppBar from "./components/AppBar/AppBar";
import todosReducer from "./reducer/todosReducer";

// "I" здесь нужна - в циклах с тудус отдельные компоненты называются туду
export interface ITodo {
    id: string,
    name: string,
    isChecked: boolean,
}

export enum todosFunctions {
    ADD = 'ADD',
    EDIT = 'EDIT',
    SELECT = 'SELECT',
    SELECT_ALL = 'SELECT_ALL',
    DELETE = 'DELETE',
}

const TodoApp = (): any => {
    const [newTaskText, setNewTaskText]: any = useState('Type new task')
    const [todos, dispatchTodos]: any = useReducer(todosReducer, exampleTodos)

    const changeInput = useCallback((e: React.FormEvent<HTMLInputElement>) => {
        setNewTaskText(e.currentTarget.value)
    }, [newTaskText])

    const onAddTodo = useCallback(() => {
        dispatchTodos({
            type: todosFunctions.ADD,
            payload: {todos, newTaskText: newTaskText}
        })
        setNewTaskText('')
    }, [newTaskText, todos, dispatchTodos]);

    const onSelectAllCheckbox = useCallback(() => {
        dispatchTodos({
            type: todosFunctions.SELECT_ALL,
            payload: {todos}
        })
    }, [todos, dispatchTodos]);

    const onDeleteSelectedTodos = useCallback(() => {
        dispatchTodos({
            type: todosFunctions.DELETE,
            payload: {todos}
        })
    }, [todos, dispatchTodos]);

    const onSelectTodo = useCallback((e: React.FormEvent<HTMLInputElement>) => {
        dispatchTodos({
            type: todosFunctions.SELECT,
            payload: {todos, id: e.currentTarget.id}
        })
    }, [todos, dispatchTodos]);

    const onEditTodo = useCallback((e: React.FormEvent<HTMLInputElement>) => {
        dispatchTodos({
            type: todosFunctions.EDIT,
            payload: {todos, id: e.currentTarget.id, value: e.currentTarget.value}
        })
    }, [todos, dispatchTodos]);


    return (
        <div className='App'>
            <div className='app__container'>
                <AppBar newTaskText={newTaskText}
                        changeInput={changeInput}
                        addTodo={onAddTodo}
                        selectAllCheckbox={onSelectAllCheckbox}
                        deleteSelectedTodos={onDeleteSelectedTodos}
                />
                <TodoList todos={todos}
                          selectTodo={onSelectTodo}
                          editTodo={onEditTodo}
                />
            </div>
        </div>
    )
}

export default React.memo(TodoApp);





