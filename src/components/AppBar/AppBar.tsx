import React from 'react';
import './AppBar.sass';
import Button from "../../primitives/Button/Button";


interface AppBarProps {
    newTaskText: string,
    changeInput: (e: React.FormEvent<HTMLInputElement>) => void,
    addTodo: () => void,
    selectAllCheckbox: () => void,
    deleteSelectedTodos: () => void,
}


function AppBar({newTaskText, changeInput, addTodo, selectAllCheckbox, deleteSelectedTodos}: AppBarProps) {
    return (
        <div className='func-line__container'>
            <div>
                <input value={newTaskText} onChange={changeInput}/>
                <Button content='Add' onClick={addTodo}/>
            </div>
            <div>
                <p>All functions:</p>
                <div className='func-buttons__container'>
                    <Button content='All' onClick={selectAllCheckbox}/>
                    <Button content='Delete' onClick={deleteSelectedTodos}/>
                </div>
            </div>
        </div>
    )
}

export default React.memo(AppBar);






