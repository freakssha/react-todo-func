import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import TodoApp from './TodoApp';

ReactDOM.render(
    <React.StrictMode>
        <TodoApp/>
    </React.StrictMode>,
    document.getElementById('root')
);

