import React from 'react';
import './TodoList.sass';
import {ITodo} from "../../TodoApp";
import TodoItem from "../../primitives/TodoItem/TodoItem";


interface TodoListProps {
    todos: ITodo[],
    selectTodo: (e: React.FormEvent<HTMLInputElement>) => void,
    editTodo: (e: React.FormEvent<HTMLInputElement>) => void,
}


function TodoList({todos, selectTodo, editTodo}: TodoListProps) {
    return (
        <div className="todo-list">
            {
                todos.map((todo: ITodo, id: number) =>
                    <TodoItem key={id} todo={todo} editTodo={editTodo} selectTodo={selectTodo}/>
                )
            }
        </div>
    )
}


export default React.memo(TodoList);


